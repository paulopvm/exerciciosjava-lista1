import javax.print.attribute.standard.NumberUp;

public class Main {

    public static void main(String[] args)
    {
        int luckNumber;
        int userNumber;
        String message;
        int tries = 1;

        NumberGenerator num = new NumberGenerator();
        luckNumber = num.luckNumber();

        do {

            UserInterface user = new UserInterface();
            userNumber = user.inputNumber();

            Comparator compare = new Comparator(luckNumber,userNumber);
            message = compare.compareNumbers();

            Printer printer = new Printer(message, tries);
            printer.print();

            tries++;


        }while(!message.equals("You Win!"));

        System.out.print("Encerrando... ");






    }
}
