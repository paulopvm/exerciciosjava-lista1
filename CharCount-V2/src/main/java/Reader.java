import java.util.Scanner;

public class Reader {

    private String word;

    public String read()
    {

        System.out.print("(exit para sair) Insira uma frase: ");

        Scanner scan = new Scanner(System.in);
        word = scan.nextLine();

        word = word.toLowerCase(); //converte para lowerCase para comparações futuras

        return word;
    }
}
