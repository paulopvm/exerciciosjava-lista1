import java.lang.Character;

public class Counter {

    public String wordToCount;

    public Counter(String wordToCount) {

        this.wordToCount = wordToCount;
    }

    public Characters charCount()
    {
        Characters wordCounted = new Characters(0,0,0,0);
        char actual;

        for (int j = 0; j < wordToCount.length(); j++) {

            //actual character in String
            actual = wordToCount.charAt(j);

            //Counting letters in general
            if(Character.isLetter(wordToCount.charAt(j)))
            {
                wordCounted.setLetters(wordCounted.getLetters()+1);
            }
            else
            {
                wordCounted.setOthers(wordCounted.getOthers()+1);

            }

            //testing regex vowels
            if(Character.toString(actual).matches("[aeiouAEIOU]"))
            {
                wordCounted.setVowels(wordCounted.getVowels()+1);
            }
            //testing regex consonants
            if(Character.toString(actual).matches("[bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ]"))
            {
                wordCounted.setConsonants(wordCounted.getConsonants()+1);
            }
        }

        return wordCounted;

    }



}
