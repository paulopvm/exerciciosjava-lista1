public class Printer {

    private Characters numberOfChar;

    public Printer(Characters numberOfChar) {
        this.numberOfChar = numberOfChar;
    }

    public void print()
    {
        //Utilizando muitos prints... versao v3 - criar uma interface e separar em classes diferentes (vogais,consoantes,etc)
        //como fazer teste unitario deste modo? - melhorias a se fazer


        //Exibe quantidade de chars
        System.out.println("Caracteres letras: " + numberOfChar.getLetters());

        //Exibe quantidade de digitos e outros
        System.out.println("Caracteres Digitos/ pontuações/ simbolos : " + numberOfChar.getOthers());

        //Exibe quantidade de vogais
        System.out.println("Caracteres Vogais: " + numberOfChar.getVowels());

        //Exibe quantidade de consoantes
        System.out.println("Caracteres Consoantes: " + numberOfChar.getConsonants());
    }
}
