public class Characters {

    public int Letters;
    public int Vowels;
    public int Consonants;
    public int Others;

    public Characters(int letters, int vowels, int consonants, int others) {
        Letters = letters;
        Vowels = vowels;
        Consonants = consonants;
        Others = others;
    }

    public int getLetters() {
        return Letters;
    }

    public void setLetters(int letters) {
        Letters = letters;
    }

    public int getVowels() {
        return Vowels;
    }

    public void setVowels(int vowels) {
        Vowels = vowels;
    }

    public int getConsonants() {
        return Consonants;
    }

    public void setConsonants(int consonants) {
        Consonants = consonants;
    }

    public int getOthers() {
        return Others;
    }

    public void setOthers(int others) {
        Others = others;
    }
}
