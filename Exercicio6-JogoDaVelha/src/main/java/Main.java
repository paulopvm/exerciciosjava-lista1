public class Main {

    public static void main(String[] args)
    {

        Jogada jogada = new Jogada();
        Jogada jogadaAtual;
        Tabuleiro tabuleiro = new Tabuleiro();
        tabuleiro.initTabuleiro();
        Jogador jogador = new Jogador(-1);

        boolean fimDeJogo = false;

        do {
            tabuleiro.exibirTabuleiro();
            jogadaAtual = jogador.jogada(jogada);
            tabuleiro.insereJogada(jogadaAtual);

        }while(fimDeJogo == false);




    }
}
