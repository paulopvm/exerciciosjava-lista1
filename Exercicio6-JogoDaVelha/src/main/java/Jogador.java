import java.util.Scanner;

public class Jogador {

    public int vezJogador;


    public Jogador(int vezJogador) {
        this.vezJogador = vezJogador; //valor inicial -1


    }

    public Jogada jogada(Jogada jogada)
    {


        Scanner scan = new Scanner(System.in);

        if(vezJogador == -1)
            System.out.println("Jogador 1 -- " + "(a,b) Insira uma posição do tabuleiro: ");
        else
            System.out.println("Jogador 2 -- " + "(a,b) Insira uma posição do tabuleiro: ");

        System.out.print("Coluna: ");
        jogada.setColuna(scan.nextInt());
        System.out.print("Linha: ");
        jogada.setLinha(scan.nextInt());
        jogada.setVezJogador(vezJogador);

        return jogada;


    }





}
