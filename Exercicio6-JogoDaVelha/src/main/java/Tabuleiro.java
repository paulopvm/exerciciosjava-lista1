public class Tabuleiro {

    public int[][] tabuleiro = new int[3][3];


    public void exibirTabuleiro()
    {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(tabuleiro[i][j]+" ");
            }
            System.out.print("\n");
        }

    }

    public void insereJogada(Jogada jogada)
    {
        if(tabuleiro[jogada.getLinha()][jogada.getColuna()] == 0)
        {
            if(jogada.vezJogador == -1)
            {
                tabuleiro[jogada.getLinha()][jogada.getColuna()] = 1;
            }
            else
            {
                tabuleiro[jogada.getLinha()][jogada.getColuna()] = 2;
            }

            System.out.println("Jogada inserida com sucesso.");
        }
        else
        {
            System.out.println("Posição já ocupada");
            jogada.vezJogador = jogada.vezJogador*-1;

        }


    }

    public void initTabuleiro()
    {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                tabuleiro[i][j] = 0;
            }
        }
    }



}
