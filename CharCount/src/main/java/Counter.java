import java.lang.Character;


public class Counter {

    public String wordToCount;

    public Counter(String wordToCount) {

        this.wordToCount = wordToCount;
    }

    public int charCount()
    {
        int wordCounted = 0;

        for (int j = 0; j < wordToCount.length(); j++) {

            if(Character.isLetter(wordToCount.charAt(j)))
            {
                wordCounted++;
            }

        }

        return wordCounted;

    }
}
