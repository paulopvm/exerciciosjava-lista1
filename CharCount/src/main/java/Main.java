public class Main {

    public static void main(String[] args)
    {
        String readWord;
        int numberOfChar;
        do {

            //Le do teclado e preenche a var readWord com  frase escrita
            Reader Keyboard = new Reader();
            readWord = Keyboard.read();

            //Le char por char da readWord e retorna quantos caracteres Letters possui
            Counter charCount = new Counter(readWord);
            numberOfChar = charCount.charCount();

            //Exibe quantidade de chars
            System.out.println("Caracteres letras: " + numberOfChar);

        }while(!readWord.equals("exit"));

        System.out.println("Encerrando... ");


    }
}
