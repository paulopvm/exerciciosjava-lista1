import java.util.Scanner;

public class UserInterface {

    public Phrase userInput()
    {
        String word1, word2;

        Scanner scan = new Scanner(System.in);

        System.out.print("Insira a primeira frase: ");
        word1 = scan.nextLine();
        System.out.print("\nInsira a segunda frase: ");
        word2 = scan.nextLine();

        Phrase userInputs = new Phrase(word1,word2);


        return userInputs;
    }

}
