import java.util.Arrays;

public class Analyser {

    Phrase words;

    public Analyser(Phrase words) {
        this.words = words;
    }

    public boolean compareWords()
    {
        String word1 = words.getWord1();
        String word2 = words.getWord2();

        word1 = word1.toLowerCase();
        word2 = word2.toLowerCase();

        //Dando problema quando utiliza -, pesquisar melhor como funciona o regex
        char[] charFromWord1 = word1.replaceAll("[\\s]","").toCharArray();
        char[] charFromWord2 = word2.replaceAll("[\\s]","").toCharArray();

        //organiza as palavras de menor para maior e as compara
        Arrays.sort(charFromWord1);
        Arrays.sort(charFromWord2);

        System.out.println(charFromWord1);
        System.out.println(charFromWord2);


        //retorna true se as palvras estiverem na mesma ordem
        return Arrays.equals(charFromWord1,charFromWord2);

    }

}
