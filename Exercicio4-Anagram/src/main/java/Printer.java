public class Printer {

    public boolean result;
    public Phrase words;

    public Printer(boolean result,Phrase words) {
        this.result = result;
        this.words = words;
    }

    public void print(){


        if(result)
            System.out.print(words.getWord1() + " e " +words.getWord2()+" SAO anagramas ");
        else
            System.out.print(words.getWord1() + " e " +words.getWord2()+" NAO SAO anagramas ");

    }
}
