public class Main {

    public static void main(String[] args)
    {

        Phrase words;
        boolean result;

        UserInterface user = new UserInterface();
        words = user.userInput();

        Analyser analyser = new Analyser(words);
        result = analyser.compareWords();

        Printer anagram = new Printer(result,words);
        anagram.print();


    }

}
